'use strict';

module.exports = app => {
  const {
    STRING,
    UUID,
    UUIDV4,
    ARRAY,
  } = app.Sequelize;

  const Roles = app.model.define('roles', {
    rolesName: {
      type: STRING,
      unique: true,
      allowNull: false,
    },
    uniqId: {
      type: UUID,
      defaultValue: UUIDV4,
      primaryKey: true,
      allowNull: false,
    },
  }, {
    ...app.config.modelCommonOption,
    indexes: [
      {
        fields: [
          'rolesName',
        ],
        unique: true,
      },
      {
        fields: [
          'uniqId',
        ],
        unique: true,
      },
    ],
  });

  return Roles;
};
