'use strict';

const Service = require('egg').Service;

class RolesService extends Service {

  async queryAllRoles(selector = {}) {
      return await this.ctx.model.Roles.findAll({
      ...selector,
      order: [
          [
          'createdAt',
          'ASC',
          ],
      ],
      });
  }

  async queryRolesByUniqId({ uniqId }) {
      return await this.ctx.model.Roles.findOne({
      where: {
          uniqId,
      },
      });
  }

  async createRoles({ rolesName }) {
      return await this.ctx.model.Roles.create({
      rolesName,
      });
  }

  async updateRoles({ uniqId, payload }) {
      return await this.ctx.model.Roles.update(
      payload,
      {
          where: {
          uniqId,
          },
      }
      );
  }

  async deleteRolesByUniqId({ uniqId }) {
      return await this.ctx.model.Roles.destroy({
      where: {
          uniqId,
      },
      });
  }
}

module.exports = RolesService;