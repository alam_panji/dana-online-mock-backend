'use strict';

const YAML = require('yamljs');
const filesize = require('filesize');
const getStream = require('get-stream');
const Controller = require('egg').Controller;

const swaggerConvert = require('../../util').swaggerConvert;

class RolesController extends Controller {

  async showAll() {
    const ctx = this.ctx;
    const res = [];
    const _res = await ctx.service.roles.queryAllRoles();
    for (const _item of _res) {
      const item = _item.get({ plain: true });
      res.push(item);
    }
    ctx.success(res);
  }

}

module.exports = RolesController;
